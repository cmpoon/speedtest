# Speed tester



Uses cloudflare speed test endpoints, at speed.cloudflare.com/__down and
speed.cloudflare.com/__up.


Uploads
```
https://speed.cloudflare.com/__up?measId=<ID>
```

Downloads
```
https://speed.cloudflare.com/__down?measId=<ID>&bytes=<BYTES>
```
