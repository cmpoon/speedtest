package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"
)

const (
	cloudflareUpload   = "https://speed.cloudflare.com/__up"
	cloudflareDownload = "https://speed.cloudflare.com/__down"
	bytes25MB          = 25000000
	bytes10MB          = 10000000
	bytes1MB           = 1000000
	bytes100KB         = 100000
	measureId          = "1234567890"
)

var uploadURL *url.URL
var downloadURL *url.URL

func init() {
	var err error
	downloadURL, err = url.Parse(cloudflareDownload)
	if err != nil {
		panic("couldn't parse download URL")
	}
	uploadURL, err = url.Parse(cloudflareUpload)
	if err != nil {
		panic("couldn't parse upload URL")
	}
}

func download(bytes int) (time.Duration, error) {
	url := *downloadURL
	q := url.Query()
	q.Set("bytes", strconv.Itoa(bytes))
	//	q.Set("measId", measureId)
	url.RawQuery = q.Encode()
	start := time.Now()
	resp, err := http.Get(url.String())
	if err != nil {
		return time.Since(start), err
	}
	defer resp.Body.Close()
	_, err = io.Copy(ioutil.Discard, resp.Body)
	return time.Since(start), err
}

func latency() (time.Duration, error) {
	return download(0)
}

func calcBW(d time.Duration, b int) float64 {
	bps := float64(b) / d.Seconds()
	return (bps / 1000000) * 8
}

type bwTest struct {
	bw int
	n  int
}

func main() {
	tests := []bwTest{
		{bytes100KB, 10},
		{bytes1MB, 10},
		{bytes10MB, 4},
		{bytes25MB, 4},
	}
	for {
		for _, t := range tests {
			wg := sync.WaitGroup{}
			start := time.Now()
			var oneError error
			once := sync.Once{}
			for i := 0; i < t.n; i++ {
				wg.Add(1)
				go func() {
					defer wg.Done()
					_, err := download(t.bw)
					once.Do(func() {
						oneError = err
					})
					//				fmt.Printf("download: %d, took: %v, bw:%fMB/s [%v]\n", bw, d, calcBW(d, bw), err)
				}()
			}
			wg.Wait()
			d := time.Since(start)
			bw := t.bw * t.n
			if oneError != nil {
				bw = 0
			}
			fmt.Printf("%s, %f, %d, %f\n", start.Format("2006/01/02 15:04:05"), calcBW(d, bw), bw, d.Seconds())
		}
		time.Sleep(time.Second * 60)
	}
}
