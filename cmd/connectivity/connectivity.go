package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const (
	cloudflareUpload   = "https://speed.cloudflare.com/__up"
	cloudflareDownload = "https://speed.cloudflare.com/__down"
)

var uploadURL *url.URL
var downloadURL *url.URL

func init() {
	var err error
	downloadURL, err = url.Parse(cloudflareDownload)
	if err != nil {
		panic("couldn't parse download URL")
	}
	uploadURL, err = url.Parse(cloudflareUpload)
	if err != nil {
		panic("couldn't parse upload URL")
	}
}

func latency() (time.Duration, error) {
	url := *downloadURL
	c := http.Client{
		Timeout: time.Second * 5,
	}
	start := time.Now()
	resp, err := c.Get(url.String())
	if err != nil {
		return time.Since(start), err
	}
	defer resp.Body.Close()
	_, err = io.Copy(ioutil.Discard, resp.Body)
	return time.Since(start), err
}

func main() {
	for {
		d, err := latency()
		if err != nil {
			fmt.Printf("%s, 0 [%v]\n", time.Now().Format("2006/01/02 15:04:05"), err)
		} else {
			fmt.Printf("%s, %d\n", time.Now().Format("2006/01/02 15:04:05"), d.Milliseconds())
		}
		time.Sleep(time.Second * 5)
	}
}
